#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "gpio.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    gpio = new GPIO(this);
    connect(gpio, &GPIO::cardDiscovered, this, &MainWindow::onKeyArrived);
    test = new GPIOTest(gpio, this);
    ui->setupUi(this);
    ui->keyUID->setText("12345");
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_openDoorButton_clicked()
{
    gpio->openDoor();
}

void MainWindow::on_closeDoorButton_clicked()
{
    gpio->closeDoor();
}

void MainWindow::onKeyArrived(QByteArray key)
{
    QString sKey;
    for (uint8_t byte: key) {
        sKey += QString("%1 ").arg(byte, 2, 16);
    }
    ui->keyUID->setText(sKey);
}

void MainWindow::on_runTestsButton_clicked()
{
    ui->listWidget->addItem(QString("makeCommand %1").arg(test->testMakeCommand()));
}

void MainWindow::on_pushButton_clicked()
{
    QByteArray data = QByteArrayLiteral("\01\01");
    gpio->writeCommand(initDeviceID, data);
}

void MainWindow::on_pushButton_2_clicked()
{
    gpio->writeCommand(getDeviceID);
}

void MainWindow::on_pushButton_3_clicked()
{
    gpio->writeCommand(getHWVision);
}

void MainWindow::on_pushButton_4_clicked()
{
    QByteArray data = QByteArrayLiteral("\01");
    gpio->writeCommand(setRFField, data);
}

void MainWindow::on_pushButton_5_clicked()
{
    QByteArray data = QByteArrayLiteral("\x26");
    gpio->writeCommand(request, data);
}

void MainWindow::on_pushButton_6_clicked()
{
    gpio->writeCommand(anticollision);
}

void MainWindow::on_pushButton_7_clicked()
{
    QByteArray data = QByteArrayLiteral("\01");
    gpio->writeCommand(setNFCField, data);
}
