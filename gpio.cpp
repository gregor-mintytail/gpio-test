#include "gpio.h"
#include <QProcess>
#include <iostream>
#include <QObject>
#include <QException>

#define HARD_DEBUG_OUTPUT false

QByteArray GPIO::makeCommand(uint16_t command, QByteArray &data)
{
    QByteArray ret;
    uint8_t csum = 0;
    // preamble
    ret.append(0xAA);
    ret.append(0xBB);
    // length (2 for device id, 2 for command, 1 for checksum)
    ret.append(data.length() + 2 + 2 + 1);
    ret.append(1, 0);
    // device id
    ret.append(RFIDID & 0xFF);
    ret.append(RFIDID >> 8);
    csum ^= (RFIDID & 0xFF) ^ (RFIDID >> 8);
    // command
    ret.append(command & 0xFF);
    ret.append(command >> 8);
    csum ^= (command & 0xFF) ^ (command >> 8);
    // data
    //ret.append(data);
    for (uint8_t i: data) {
        ret.append(i);
        csum ^= i;
        /**
         * If there is any byte equaling to AA occurs between Len and Checksum, one byte 00 will
         * be added after this byte to differentiate preamble. However, the Len keeps unchanged.
         **/
        if (0xAA == i) {
            ret.append(1, 0);
        }
    }
    // control sum
    ret.append(csum);
    return ret;
}

void GPIO::writeCommand(SL060Commands command, QByteArray &data)
{
    if (noCommand == command) return;

    QByteArray bytes = makeCommand(command, data);
    bytesWritten = serialPort.write(bytes);
#if HARD_DEBUG_OUTPUT == true
    QString output;
    for (uint8_t i: bytes) {
        output += QString("%1 ").arg(i, 2, 16);
    }
    std::cout << "command sent " << output.toStdString() << ", total bytes " << bytesWritten << std::endl;
#endif
    if (bytesWritten == -1) {
        std::cerr << QObject::tr("GPIO::writeCommand bytesWritten == -1 port %1, error: %2")
                            .arg(serialPort.portName())
                            .arg(serialPort.errorString())
                            .toStdString()
                         << std::endl;
        throw new QException();
    } else if (bytesWritten != bytes.size()) {
        std::cerr << QObject::tr("GPIO::writeCommand bytesWritten != bytes.size() port %1, error: %2\nbytes.size %3, bytesWritten %4")
                            .arg(serialPort.portName())
                            .arg(serialPort.errorString())
			    .arg(bytes.size())
			    .arg(bytesWritten)
                            .toStdString()
                         << std::endl;
        throw new QException();
    }
    responseWaiting = command;
    writeTimeout.start();
}

void GPIO::writeCommand(SL060Commands command)
{
    QByteArray empty;
    writeCommand(command, empty);
}

GPIO::GPIO(QObject *parent) : QObject(parent)
{
    RFIDID = 0;
    QString portname = "/dev/ttyAMA0";
    serialPort.setPortName(portname);
    serialPort.setBaudRate(QSerialPort::Baud9600);
    serialPort.setParity(QSerialPort::NoParity);
    serialPort.setStopBits(QSerialPort::OneStop);
    serialPort.setFlowControl(QSerialPort::NoFlowControl);
    if (!serialPort.open(QIODevice::ReadWrite)) {
        std::cerr << QObject::tr("Failed to open port %1, error: %2")
                          .arg(portname)
                          .arg(serialPort.errorString())
                          .toStdString()
                       << std::endl;
        //throw new QException();
    }
    connect(&serialPort, &QSerialPort::readyRead, this, &GPIO::handleReadyRead);//*/
    connect(&serialPort, &QSerialPort::bytesWritten, this, &GPIO::handleBytesWritten);
    //connect(&serialPort, &QSerialPort::errorOccurred, this, &GPIO::handleError);
    /// timeout for unexpected response
    bufferTimeout.setSingleShot(true);
    bufferTimeout.setInterval(5000);
    connect(&bufferTimeout, &QTimer::timeout, this, &GPIO::bufferTimeouted);
    /// wait for response from rfid controller
    writeTimeout.setSingleShot(true);
    writeTimeout.setInterval(2000);
    connect(&writeTimeout, &QTimer::timeout, this, &GPIO::writeTimeouted);
    /// poll for new rfid interval
    scanTimer.setInterval(300);
    scanTimer.setSingleShot(false);
    connect(&scanTimer, &QTimer::timeout, this, &GPIO::scanTimeouted);
    scanTimer.start();
    /// setup here all the pins used in the project
    shell.execute("gpio", {"mode", QString("%1").arg(DOOR_PIN), "output"});
    /// turn on RF field
    QByteArray command = QByteArrayLiteral("\x01");
    writeCommand(setRFField, command);
}

void GPIO::setPin(int pin, bool state)
{
    shell.execute("gpio", {"write", QString("%1").arg(pin), (state)? "1": "0"});
}

void GPIO::openDoor()
{
    setPin(DOOR_PIN, true);
}

void GPIO::closeDoor()
{
    setPin(DOOR_PIN, false);
}

void GPIO::handleError(QSerialPort::SerialPortError serialPortError)
{
    if (serialPortError == QSerialPort::ReadError || serialPortError == QSerialPort::WriteError) {
        std::cerr << QObject::tr("GPIO::handleError port %1, error: %2")
                            .arg(serialPort.portName())
                            .arg(serialPort.errorString())
                            .toStdString()
                         << std::endl;
        throw new QException();
    } else {
        std::cerr << QObject::tr("GPIO::handleError non-critical error port %1, error: %2")
                     .arg(serialPort.portName())
                     .arg(serialPort.errorString())
                     .toStdString()
                  << std::endl;
    }
}

void GPIO::handleReadyRead()
{
    QByteArray chunk(serialPort.readAll());
    for (uint8_t i: chunk) {
        switch (bufferState) {
        case start: {
#if HARD_DEBUG_OUTPUT == true
            std::cout << std::endl;
#endif
            if (0xAA == i) bufferState = prefix2;
            else bufferState = error;
            break;
        }
        case prefix2: {
            if (0xBB == i) bufferState = length1;
            else bufferState = error;
            break;
        }
        case length1: {
            bufferLength = i;
            bufferState = length2;
            break;
        }
        case length2: {
            bufferLength |= i << 8;
            bufferState = payload;
            buffer.clear();
            bufferChecksum = 0;
            break;
        }
        case payload: {
            if (0 < buffer.length() && (char) 0xAA == buffer.at(buffer.length()-1) && 0x00 == i) break;
            buffer.append(i);
            bufferChecksum ^= i;
            // checksum is the part of the payload actually
            if (buffer.length() >= bufferLength-1) bufferState = checksum;
            break;
        }
        case checksum: {
#if HARD_DEBUG_OUTPUT == true
            std::cout << std::endl;
#endif
            writeTimeout.stop();
            if (bufferChecksum != i) bufferState = error;
            else {
                // read response
                uint8_t status = buffer[4];
                SL060Commands currentResponse = responseWaiting;
                responseWaiting = noCommand;
                switch (currentResponse) {
                default: {
                    QString sResponse;
                    for (uint8_t byte: buffer) {
                        sResponse += QString("%1 ").arg(byte, 2, 16);
                    }
                    std::cerr << "undefined command <"
                              << currentResponse
                              << "> command "
                              << sResponse.toStdString()
                              << std::endl;
                    break;
                }
                case initDeviceID: {
                    if (RFIDStatusOk == status) {
                        writeCommand(anticollision);
                    } else if (RFIDStatusNoCard == status) {
                        // erase buffer — so we can emit signal for the same card more, than once
                        cardUID = QByteArrayLiteral("");
                    } else {
                        QString sResponse;
                        for (uint8_t byte: buffer) {
                            sResponse += QString("%1 ").arg(byte, 2, 16);
                        }
                        std::cerr << "unknown response for initDeviceID <"
                                  << currentResponse
                                  << "> command "
                                  << sResponse.toStdString()
                                  << std::endl;
                    }
                    break;
                }
                case anticollision: {
                    // DeviceID 2 bytes, command 0x0202, status 1 byte, uid 4-7 bytes
                    /// cut first 5 bytes from the buffer
                    QByteArray uid = buffer.right(buffer.length()-5);
#if HARD_DEBUG_OUTPUT == true
                    QString output;
                    for (uint8_t i: uid) {
                        output += QString("%1 ").arg(i, 2, 16);
                    }
                    std::cout << "Key discovered <" << output.toStdString() << ">" << std::endl;
#endif
                    if (uid != cardUID) {
                        cardUID = uid;
                        // if no card discovered, status will be RFIDStatusFailed
                        if (RFIDStatusOk == status) {
                            QString output;
                            for (uint8_t i: cardUID) {
                                output += QString("%1 ").arg(i, 2, 16);
                            }
                            std::cout << "Key discovered <" << output.toStdString() << ">" << std::endl;
                            emit cardDiscovered(cardUID);
                        }
                    }
                    break;
                }
                case getDeviceID: {
                    if (RFIDStatusOk == status) {
                        RFIDID = buffer[5] | (buffer[6] << 8);
                        scanTimer.start();
                    } else {
                        std::cerr << "Achtung! Reader responded with not ok status <"
                                  << QString("%1").arg(status, 4, 16).toStdString()
                                  << "> to getDeviceID command. That was unexpected."
                                  << std::endl;
                    }
                    break;
                }
                }
            }
            buffer.clear();
            bufferState = start;
            break;
        }
        default:
        case error: {
            // do nothing, wait timeout
            std::cerr << "GPIO::handleReadyRead parser in error state" << std::endl;
            if (! bufferTimeout.isActive()) bufferTimeout.start();
            break;
        }
        }
#if HARD_DEBUG_OUTPUT == true
        std::cout << QString("%1 ").arg(i, 2, 16).toStdString();
#endif
    }
}

void GPIO::handleBytesWritten(qint64 bytes)
{
#if HARD_DEBUG_OUTPUT == true
    std::cout << "bytes written " << bytes << std::endl;
#endif
    bytesWritten -= bytes;
    if (0 >= bytesWritten) {
        writeTimeout.stop();
    }
}

void GPIO::bufferTimeouted()
{
    std::cerr << "buffer timeouted" << std::endl;
    bufferState = start;
}

void GPIO::writeTimeouted()
{
    std::cerr << QObject::tr("GPIO::writeTimeouted port %1, error: %2\nCommand was <%3>")
                        .arg(serialPort.portName())
                        .arg(serialPort.errorString())
                        .arg(responseWaiting, 4, 16)
                        .toStdString()
              << std::endl;
    responseWaiting = noCommand;
}

void GPIO::scanTimeouted()
{
    writeCommand(initDeviceID);
}
