#ifndef GPIOTEST_H
#define GPIOTEST_H

#include <QObject>
#include "gpio.h"
#include <iostream>

#define test(condition, message) \
    if (! (condition)) { \
        std::cerr << "test failed with message <" << QString(message).toStdString() << ">" << std::endl; \
        return false; \
    }

class GPIOTest : public QObject
{
    Q_OBJECT
    GPIO *gpio;
public:
    explicit GPIOTest(GPIO *gpio, QObject *parent = nullptr);
    bool testMakeCommand();

signals:

public slots:
};

#endif // GPIOTEST_H
