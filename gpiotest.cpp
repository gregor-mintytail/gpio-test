#include "gpiotest.h"

GPIOTest::GPIOTest(GPIO *gpio, QObject *parent) : QObject(parent)
{
    this->gpio = gpio;
}

bool GPIOTest::testMakeCommand()
{
    QByteArray data, encodedData;
    data.append('\0').append('\1').append('\xAA').append('\xAA').append('\0');
    encodedData.append('\0').append('\1').append('\xAA').append('\0').append('\xAA').append('\0').append('\0');
    gpio->RFIDID = 9999;
    QByteArray ret = gpio->makeCommand(65500, data);
    test((char) 0xAA == ret[0] && (char) 0xBB == ret[1], "command prefix is not 0xAA 0xBB");
    test(10 == ret[2], "length of command should be 10");
    test(9999 == (ret[4] | ret[5] << 8), "Device ID is wrong");
    test(65500 == (uint16_t)(ret[6] | ret[7] << 8), "command is wrong");
    for (uint8_t i = 0; i < encodedData.size(); i++) {
        test(ret.size() > i+8, "data is too short");
        test(encodedData[i] == ret[i+8], "data encoded wrong");
    }
    test(0x0A == ret[15], "checksum do not match");
    return true;
}
